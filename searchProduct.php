<?php
session_start();
include("components/dbconnect.php");
include("components/functions.php");

$output = '';

if(isset($_GET['searchVal']))
{
    $searchquery = $_GET['searchVal'];

    if(strlen($searchquery) > 1) {
    $counter = 0;
    $searchquery = "%$searchquery%";
    $stmt = $db->prepare("SELECT id, name, price_eur FROM products WHERE name LIKE ? LIMIT 5");
    $stmt->bind_param('s', $searchquery);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($id, $name, $price);
    if($stmt->num_rows == 0)
    {
        $output = '<div class="founderror"> No products found! </div>';
    }
    else
    {
        while($stmt->fetch() and $counter < 6)
        {
            {
                $path = "pictures/".getPathById($id);
                $output .= '<div class="result-line">';
                $output .= '<a href="product.php?prod='.$id.'" >';
                $output .= '<div class="search-result-image"><img id="search-pic" src="'.$path.'"/></div>';
                $output .= '<div class="search-result-details">';
                $output .= '<span>'.$name.' </span><br>';
                $output .= '<span>'.$price.' Euro</span>';
                $output .= '</div>';
                $output .= '</a></div>';
            }
            $counter ++;
        }
    }
    $stmt->close();
    }
    echo($output);
}
?>
