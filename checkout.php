<?php
  include("components/header.php");
  checkCheckout();
?>

<div class="checkout-box">

  <!-- 1st column -->
  <div class="checkout-col cartbox-left">
    <p class="headline">Check your order</p>
    <?php
    $noProducts = true;
    $totalprice = 0;
    $productsArray = array();
    if(isset($_SESSION['products'])){
      $productsArray = $_SESSION['products'];
      if(!empty($productsArray)) {
        $noProducts = false;
        $cartItemsReadOnly = true; //used in cartItem.php
        foreach ($productsArray as $id => $qnty) {
          include("components/cartItem.php");
        }
      }
    }
    if ($noProducts) { ?>
      <p><a href='home.php'>Click here</a> to find products.</p>
    <?php } ?>
  </div>

  <!-- 2nd column -->
  <div class="checkout-col">
    <p class="headline">Check personal data</p>
    <?php
    if(isset($_SESSION['user'])){
      $user = getUserDetails($_SESSION['user']);
    }
    $email = isset($user['email']) ? $user['email'] : "";
    $firstname = isset($user['firstname']) ? $user['firstname'] : "";
    $lastname = isset($user['lastname']) ? $user['lastname'] : "";
    $town = isset($user['town']) ? $user['town'] : "";
    $zipcode = isset($user['zipcode']) ? $user['zipcode'] : "";
    $street = isset($user['street']) ? $user['street'] : "";
    $housenumber = isset($user['housenumber']) ? $user['housenumber'] : "";
    $deliverymethod = isset($user['deliverymethod']) ? $user['deliverymethod'] : "";
    $creditnumber = isset($user['creditnumber']) ? $user['creditnumber'] : "";
    ?>
    <div class="personaldetails-box">
      <label for="email">Email</label>
      <p><?= $email ?></p>
      <label for="firstname">Firstname</label>
      <p><?= $firstname ?></p>
      <label for="lastname">Lastname</label>
      <p><?= $lastname ?></p>
      <label for="town">Town</label>
      <p><?= $town ?></p>
      <label for="zipcode">Zipcode</label>
      <p><?= $zipcode ?></p>
      <label for="street">Street</label>
      <p><?= $street ?></p>
      <label for="housenumber">Housenumber</label>
      <p><?= $housenumber ?></p>
      <label for="deliverymethod">Deliverymethod</label>
      <p><?php echo $deliverymethod == "dhl" ? "DHL" : "Express"; ?></p>
      <label for="creditnumber">Creditcard</label>
      <p>ends with: <?= substr($creditnumber, -4) ?></p>
    </div>
    <form class="" action="personal_details.php" method="post">
      <input class="btnStandard" type="submit" name="submitpersonal" value="Change personal data">
    </form>
  </div>

  <!-- 3rd column -->
  <div class="checkout-col pay">
    <p class="headline">Finalize your order</p>
    <div>Total <?= $totalprice ?> Euro</div>
    <form class="" action="createOrder.php" method="post">
      <input class="btnStandard" type="submit" name="pay" value="Confirm payment">
    </form>
    <?php
    if (isset($_SESSION["errorOrder"])) {
      echo "<div style='color:red'>".$_SESSION["errorOrder"]."</div>";
    }
    ?>
  </div>

</div>

<?php include("components/footer.php"); ?>
