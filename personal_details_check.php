<?php
session_start();
include("components/dbconnect.php");
include("components/functions.php");

$tmpUser = array();
$tmpUser['email'] = $_POST['email'];
$tmpUser['firstname'] = $_POST['firstname'];
$tmpUser['lastname'] = $_POST['lastname'];
$tmpUser['town'] = $_POST['town'];
$tmpUser['zipcode'] = $_POST['zipcode'];
$tmpUser['street'] = $_POST['street'];
$tmpUser['housenumber'] = $_POST['housenumber'];
$tmpUser['creditnumber'] = $_POST['creditnumber'];
$tmpUser['securecode'] = $_POST['securecode'];
$tmpUser['deliverymethod'] = $_POST['deliverymethod'];

$_SESSION['tmpUser'] = $tmpUser;

if(isset($_POST['email']) and isset($_POST['firstname'])
  and isset($_POST['lastname']) and isset($_POST['town'])
  and isset($_POST['zipcode']) and isset($_POST['street'])
  and isset($_POST['housenumber']) and isset($_POST['creditnumber'])
  and isset($_POST['securecode']) and isset($_POST['deliverymethod'])){

  $id = 0;
  if (isset($_SESSION['user'])) {
    $id = $_SESSION['user'];
  }

  $email = trim($_POST['email']);
  $firstname = trim($_POST['firstname']);
  $lastname = trim($_POST['lastname']);
  $town = trim($_POST['town']);
  $zipcode = trim($_POST['zipcode']);
  $street = trim($_POST['street']);
  $housenumber = trim($_POST['housenumber']);
  $creditnumber = trim($_POST['creditnumber']);
  $securecode = trim($_POST['securecode']);
  $deliverymethod = trim($_POST['deliverymethod']);
  $errorCnt = 0;

  $errorMsg = "";
  if((strlen($email) <= 5) || (strlen($email) > 100)){
    $errorCnt++;
    $errorMsg .= "Email needs to be between 5 and 100 signs long.\\n";
  }
  if((strlen($firstname) <= 2) || (strlen($firstname) > 50)){
    $errorCnt++;
    $errorMsg .= "Firstname needs to be between 2 and 50 signs long.\\n";
  }
  if(strlen($lastname) <= 2 || strlen($lastname) > 50){
    $errorCnt++;
    $errorMsg .= "Lastname needs to be between 2 and 50 signs long.\\n";
  }
  if(strlen($town) <= 2 || strlen($town) > 50){
    $errorCnt++;
    $errorMsg .= "Town needs to be between 2 and 50 signs long.\\n";
  }
  if(strlen($zipcode) <= 2 || strlen($zipcode) > 16){
    $errorCnt++;
    $errorMsg .= "Zipcode needs to be between 2 and 16 signs long.\\n";
  }
  if(strlen($street) <= 2 || strlen($street) > 100){
    $errorCnt++;
    $errorMsg .= "Street needs to be between 2 and 100 signs long.\\n";
  }
  if(strlen($housenumber) <= 1 || strlen($housenumber) > 5 ){
    $errorCnt++;
    $errorMsg .= "Housenumber needs to be between 1 and 5 signs long.\\n";
  }
  if(strlen($creditnumber) <= 11 || strlen($creditnumber) > 20 ){
    $errorCnt++;
    $errorMsg .= "Creditcard number needs to be between 11 and 20 signs long.\\n";
  }
  if(strlen($securecode) != 3){
    $errorCnt++;
    $errorMsg .= "Secure code needs to be exactly 3 signs long.\\n";
  }
  if(!($deliverymethod == 'dhl' || $deliverymethod == 'express')){
    $errorCnt++;
    $errorMsg .= "Delivery method wrong, choose one of the available options.\\n";
  }

  if($errorCnt == 0){
    if (!userexists($id)){
      $stmt = $db->prepare("INSERT INTO users (email, firstname, lastname, town, zipcode,
        street, housenumber, deliverymethod, creditnumber, securecode) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
      $stmt->bind_param('ssssssssss', $email, $firstname, $lastname, $town, $zipcode,
        $street, $housenumber, $deliverymethod, $creditnumber, $securecode);
    } else {
      $stmt = $db->prepare("UPDATE users SET email=?, firstname=?, lastname=?, town=?, zipcode=?,
        street=?, housenumber=?, deliverymethod=?, creditnumber=?, securecode=? WHERE id=?");
      $stmt->bind_param('ssssssssssi', $email, $firstname, $lastname, $town, $zipcode,
        $street, $housenumber, $deliverymethod, $creditnumber, $securecode, $id);
    }
    $stmt->execute();
    $stmt->close();
    if ($db->insert_id > 0) {
      $_SESSION['user'] = $db->insert_id;
    }
    unset($_SESSION['tmpUser']);
    unset($_SESSION['errorPersonalDetails']);
    header("Location: checkout.php");
  } else {
    $_SESSION['errorPersonalDetails'] = $errorMsg;
    header("Location: personal_details.php");
  }
}
else {
  header("Location: personal_details.php");
}

?>
