<?php
include("dbconnect.php");
include("product_class.php");


function makeProductForm($id, $name, $details, $stock, $price, $path) {
  $productpage = "product.php?prod=".$id;
  $path = "pictures/".$path;
  echo '<div class="checkboard">';
  echo '<span class="picture"><a href="'.$productpage.'"><img src="'.$path.'" alt="Picture"/></a> <br/> </span>';
	echo '<div class="name"> <a href="'.$productpage.'">'.$name.'</a> </div>';
  echo '<p>'.$price. ' Euro<p/>';
  echo '</div>';
}

function getPathById($id) {
  global $db;
  $picPath = "picturenotfound.png";
  $stmt = $db->prepare("SELECT picpath FROM Pictures WHERE product_id = ? LIMIT 1");
  $stmt->bind_param('i', $id);
	$stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($path);
  $stmt->fetch();
  if($stmt->num_rows == 1)
	{
		$picPath = $path;
	}
	$stmt->free_result();
	$stmt->close();

  return $picPath;
}

function getProductsPageCount($cat_id) {
	global $db;
	$stmt = $db->prepare("SELECT count(*) FROM products WHERE category_id = ?");
  $stmt->bind_param('i', $cat_id);
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($cnt);
  $stmt->fetch();
  $result = 0;
  if ($stmt->num_rows == 1) {
    $result = $cnt;
  }
	$stmt->free_result();
	$stmt->close();
  return ceil($result/12);
}

function getProducts($cat_id, $page) {
	global $db;
  $page *= 12;
	$stmt = $db->prepare("SELECT id, name, details, stock, price_eur FROM products WHERE category_id = ? ORDER BY id LIMIT 12 OFFSET ?");
  $stmt->bind_param('ii', $cat_id, $page);
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($id, $name, $details, $stock, $price);
	while($stmt->fetch())
  {
    $path = getPathById($id);
    makeProductForm($id, $name, $details, $stock, $price, $path);
  }
	$stmt->free_result();
	$stmt->close();
}

function getCatheadline(){
  $catheadline = "";
  if(isset($_GET['cat'])){
  $cat = htmlspecialchars($_GET['cat']);
    switch ($cat) {
    case 'wristbands':
            $catheadline = "Wristbands";
      break;

    case 'lighters':
            $catheadline = "Lighters";
      break;

    case 'swissknives':
            $catheadline = "Swiss Knives";
      break;

    case 'watches':
            $catheadline = "Watches";
      break;

    default:
            header("Location: error.php");
      break;
    }
    return $catheadline;
  }
}

function getRandomProduct(){
  global $db;
  $i = 0;
  $j = 0;
  $arraycatids = array();
  $stmt = $db->prepare("SELECT id FROM categories ORDER BY id");
  $stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($id);
  while($stmt->fetch())
  {
    $arraycatids[$j++] = $id;
  }
  $stmt->free_result();
  $stmt->close();

  while($i < count($arraycatids)){
    $cat_id = $arraycatids[$i];
    $stmt = $db->prepare("SELECT id, name, details, stock, price_eur
      FROM products WHERE category_id = ? ORDER BY stock LIMIT 3");
    $stmt->bind_param('i', $cat_id);
  	$stmt->execute();
  	$stmt->store_result();
  	$stmt->bind_result($id, $name, $details, $stock, $price);
  	while($stmt->fetch())
    {
      $path = getPathById($id);
      makeProductForm($id, $name, $details, $stock, $price, $path);
    }
  	$stmt->free_result();
  	$stmt->close();
    $i++;
  }
}

function getProductById($id) {
  global $db;
  $product = new product();
  $stmt = $db->prepare("SELECT id, name, details, stock, price_eur
    FROM products WHERE id = ? LIMIT 1");
  $stmt->bind_param('i', $id);
  $stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($id, $name, $details, $stock, $price);
  $stmt->fetch();
  if($stmt->num_rows == 1)
  {
    $product->addAll($id, $name, $details, $stock, $price);
  }
  $stmt->free_result();
  $stmt->close();

  return $product;
}

function productExistsById($id){
  global $db;
  $exists = false;
  $stmt = $db->prepare("SELECT id FROM products WHERE id = ? LIMIT 1");
  $stmt->bind_param('i', $id);
  $stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($id);
  $stmt->fetch();
  if($stmt->num_rows == 1)
  {
    $exists = true;
  }
  $stmt->free_result();
  $stmt->close();

  return $exists;
}

function getUserDetails($id){
  global $db;
  $user = array();
  $stmt = $db->prepare("SELECT id, email, firstname, lastname, town, zipcode, street,
    housenumber, deliverymethod, creditnumber, securecode FROM users WHERE id = ? LIMIT 1");
  $stmt->bind_param('i', $id);
  $stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($id, $email, $firstname, $lastname, $town, $zipcode, $street,
  $housenumber, $deliverymethod, $creditnumber, $securecode);
  $stmt->fetch();
  if($stmt->num_rows == 1)
  {
    $user = array('id' => $id, 'email' => $email, 'firstname' => $firstname,
    'lastname' => $lastname, 'town' => $town, 'zipcode' => $zipcode,
    'street' => $street, 'housenumber' => $housenumber,
    'deliverymethod' => $deliverymethod, 'creditnumber' => $creditnumber,
    'securecode' => $securecode);
  }
  $stmt->free_result();
  $stmt->close();
  return $user;
}

function userexists($id){
  global $db;
  $exists = false;
  $stmt = $db->prepare("SELECT id FROM users WHERE id = ? LIMIT 1");
  $stmt->bind_param('i', $id);
  $stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($id);
  $stmt->fetch();
  if($stmt->num_rows == 1)
  {
    $exists = true;
  }
  $stmt->free_result();
  $stmt->close();

  return $exists;
}

function informAvailability($stock){
  switch ($stock) {
    case $stock <= 5:
        $info = $stock.' products left.';
        break;

    default:
        $info = 'Available.';
        break;
  }
return $info;
}

function setAvailabilityColor($stock){
  switch ($stock) {
    case $stock <= 5:
        $color = 'red';
        break;

    default:
        $color = 'green';
        break;
  }
return $color;
}

// Error handling for a checkout
function checkCheckout() {
  $productsArray = array();
  if (isset($_SESSION['products'])) {
    $productsArray = $_SESSION['products'];
  }
  if (isset($_SESSION['user']) && !empty($productsArray)) {
    if (checkAndAdjustQuantities($productsArray)){
      unset($_SESSION["errorOrder"]);
    } else {
      $_SESSION['errorOrder'] = "The chosen quantities exceed the available amount. They have been reset to the maximum available amount.";
    }
  } else {
    $_SESSION['errorOrder'] = "Unknown error occurred.";
    if (empty($productsArray)) {
      $_SESSION["errorOrder"] = "No products in cart.";
    } else if (!isset($user)) {
      $_SESSION["errorOrder"] = "A problem with the personal details occurred.";
    }
    return false;
  }
  return true;
}

function checkAndAdjustQuantities($orderItems) {
  $available = true;
  $newOrderItems = array();
  foreach ($orderItems as $id => $qnty) {
    $product = getProductById($id);
    if ($product->get_stock() < $qnty) {
      $newOrderItems[$id] = $product->get_stock();
      $available = false;
    } else {
      $newOrderItems[$id] = $qnty;
    }
  }
  if (!$available) {
    $_SESSION["products"] = $newOrderItems;
  }
  return $available;
}

function createOrderFromCart($user, $productsWithQnty) {
  global $db;
  $query = "INSERT INTO orders (user_id) VALUES (?)";
  $stmt = $db->prepare($query);
  $stmt->bind_param('i', $user["id"]);
  $stmt->execute();
  $orderId = $db->insert_id;
	$stmt->close();
  if ($orderId > 0) {
    debug_to_console("quantity of different prods: ".count($productsWithQnty));
    foreach ($productsWithQnty as $id => $qnty) {
      $query = "UPDATE products SET stock=? WHERE id=?";
      $stmt = $db->prepare($query);
      $newStock = getProductById($id)->get_stock() - $qnty;
      $stmt->bind_param('ii', $newStock, $id);
      $stmt->execute();
      $stmt->close();
      $query = "INSERT INTO order_details (order_id, product_id, quantity) VALUES (?,?,?)";
      $stmt = $db->prepare($query);
      $stmt->bind_param('iii', $orderId, $id, $qnty);
      $stmt->execute();
    	$stmt->close();
    }
  }
	return $orderId;
}

function getLastOrderOfUser($userId) {
  global $db;
  $query = "SELECT id FROM orders WHERE user_id=? ORDER BY id DESC LIMIT 1";
  $stmt = $db->prepare($query);
  $stmt->bind_param('i', $userId);
  $stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($orderId);
  $stmt->fetch();
  $stmt->free_result();
  $stmt->close();
  $products = array();
  $query = "SELECT product_id, quantity FROM order_details WHERE order_id=? ORDER BY product_id DESC";
  $stmt = $db->prepare($query);
  $stmt->bind_param('i', $orderId);
  $stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($product_id, $qnty);
  while($stmt->fetch()) {
    $products[$product_id] = $qnty;
  }
  $stmt->free_result();
  $stmt->close();
  return array('id' => $orderId, 'products' => $products);
}

// source: http://stackoverflow.com/questions/16120822/mysqli-bind-param-expected-to-be-a-reference-value-given
function refValues($arr){
    if (strnatcmp(phpversion(),'5.3') >= 0) //Reference is required for PHP 5.3+
    {
        $refs = array();
        foreach($arr as $key => $value)
            $refs[$key] = &$arr[$key];
        return $refs;
    }
    return $arr;
}

/**
 * Send debug code to the Javascript console
 */
function debug_to_console($data) {
  if(is_array($data) || is_object($data)) {
    echo("<script>console.log('PHP: ".json_encode($data)."');</script>");
  } else {
    echo("<script>console.log('PHP: $data');</script>");
  }
}
?>
