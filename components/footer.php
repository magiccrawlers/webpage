  </div>
  <footer>
    <div id="footer">
      <br>
      MensExtras - the accessory shop for men.<br>
      <br>
      This is a project for the course Advanced Internet Technologies<br>
      at<br>
      Free University of Bolzano <br>
      <br>
      Copyright (c) 2017 Copyright Fink Danilo & Meisterernst Marius <br>
      All Rights Reserved. <br>
      <br>
      Disclaimer: none of the shown products is sold in reality,
      as this is a fictious shop for the purpose of studying.
    </div>
  </footer>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script type="text/javascript" src="js/changeQuantity.js"></script>
  <script type="text/javascript" src="js/header.js"></script>
  <script type="text/javascript" src="js/search.js"></script>
</body>
</html>
