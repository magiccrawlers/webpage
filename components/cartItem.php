<?php
  if (!isset($cartItemsReadOnly)) {
    $cartItemsReadOnly = false;
  }
  $product = getProductById($id);
  $path = "pictures/".getPathById($id);
  $price = $product->get_price();
  $totalpriceproduct = $price * $qnty;
  $totalprice += $totalpriceproduct;
  $stock = $product->get_stock();
  if(!$cartItemsReadOnly && $stock < $qnty) {
    $qnty = $stock;
  }
?>
<div class="product-row">
  <div class="product-inner-row-left">
    <a href="product.php?prod=<?=$id?>" >
      <img id="product-pic" src="<?=$path?>"/>
    </a>
  </div>
  <div class="product-inner-row-right">
    <div class="cart-content prod-name-cart-page">
      <a href="product.php?prod=<?=$id?>"><?=$product->get_name()?></a>
    </div>
    <div class="cart-content" for="qnty">Quantity:
      <?php if (!$cartItemsReadOnly) { ?>
        <select name="qnty" autocomplete="off" id="qnty<?=$id?>" onchange="changeQuantity(<?=$id?>, <?= $stock?>)" tabindex="-1">
          <?php for ($j=1; $j <= $stock; $j++) { ?>
            <option value=<? echo $j ?> <?php echo $j==$qnty ? "selected" : ""; ?>><? echo $j ?></option>
          <?php } ?>
        </select>
      <?php } else {
        echo $qnty;
    } ?>
    </div>
    <div class="cart-content">Price: <?=$price?> Euro</div>
    <div class="cart-content">Subtotal: <span id="subtotal<?=$id?>"><?=$totalpriceproduct?></span> Euro</div>
    <?php if (!$cartItemsReadOnly) { ?>
      <div class="cart-content">
        <form action="remove.php?prod=<?= $id ?>" method="post">
          <input class="btnNegative btnS" type="submit" name="removeproduct" value="Remove" onClick="return confirm('Do you really want to remove this product from your cart?')">
        </form>
      </div>
      <?php
    } ?>
  </div>
</div>
