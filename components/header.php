<?php
session_start();
include("dbconnect.php");
include("functions.php");
?>

<!DOCTYPE html"-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>MensExtras</title>
  <link href="css/cart.css" rel="stylesheet" type="text/css" />
  <link href="css/checkout.css" rel="stylesheet" type="text/css" />
  <link href="css/order.css" rel="stylesheet" type="text/css" />
  <link href="css/content.css" rel="stylesheet" type="text/css" />
  <link href="css/footer.css" rel="stylesheet" type="text/css" />
  <link href="css/header.css" rel="stylesheet" type="text/css" />
  <link href="css/category.css" rel="stylesheet" type="text/css" />
  <link href="css/product.css" rel="stylesheet" type="text/css" />
  <link href="css/search.css" rel="stylesheet" type="text/css" />
  <link href="css/personal_details.css" rel="stylesheet" type="text/css" />
</head>
<body id="mc">

  <nav>
    <ul class="navBar">
      <li id="logo"><a href="home.php">MensExtras</a></li>
      <li id="logoMobile"><a href="home.php">Mens<br>Extras</a></li>

      <li id="search">
        <input id="searchInput" type="text" class ="search-navbar" autocomplete="off" name="search" maxlength="50" size="40"
          placeholder="Find Products" onkeyup="getSearchResult(this.value);"/>
        <div id="navoutput" class="dropdown-search-navbar"></div>
      </li>

      <li id="menu-icon">
        <img class="menu navBarImg" src="images/menu-icon.png" alt="Menu">
      </li>

      <div class="dropdownMobile">
        <li id="navMenu">
          <ul>
            <li><a href="home.php">Home</a></li>
            <li>
              <div class="dropdown">
                <a class="dropbtn">Categories</a>
                <div class="dropdown-content">
                  <a href="category.php?cat=wristbands">Wristbands</a>
                  <a href="category.php?cat=lighters">Lighters</a>
                  <a href="category.php?cat=swissknives">Swiss Knives</a>
                  <a href="category.php?cat=watches">Watches</a>
                </div>
              </div>
            </li>
          </ul>
        </li>

        <li id="cart-icon">
          <a href="cart.php">
            <img class="navBarImg" src="images/basket.png" alt="Shopping Cart">
          </a>
        </li>
      </div>
    </ul>
  </nav>

  <div class="main">
