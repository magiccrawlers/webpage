<?php
class product {
  var $id = "";
  var $name = "";
  var $details = "";
  var $stock = "";
  var $price = "";

    function set_id($new_id) {
        $this->id = $new_id;
 		}
    function set_name($new_name) {
   			$this->name = $new_name;
    }
    function set_details($new_details) {
        $this->details = $new_details;
 		}
    function set_stock($new_stock) {
			   $this->stock = $new_stock;
		}
    function set_price($new_price) {
			   $this->price = $new_price;
 		}

    function get_id() {
			return $this->id;
		}
    function get_name() {
			return $this->name;
		}
    function get_details() {
			return $this->details;
		}
    function get_stock() {
			return $this->stock;
		}
    function get_price() {
			return $this->price;
		}

    function addAll($new_id, $new_name, $new_details, $new_stock, $new_price) {
      $this->set_id($new_id);
      $this->set_name($new_name);
      $this->set_details($new_details);
      $this->set_stock($new_stock);
      $this->set_price($new_price);
    }
}
?>
