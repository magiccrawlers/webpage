<?php
  include("components/header.php");
?>

<div class="order-box">
  <?php
  if (isset($_SESSION["user"])) {
    $userId = $_SESSION["user"];
    $order = getLastOrderOfUser($userId);
    if (isset($order['products']) && $order["id"]) {
      $orderProducts = $order['products'];
      ?>
      <p class="headline">Order Nr. <?=$order['id'] ?> successful!</p>
      <div class="order-innerbox">
        <div class="order-col cartbox-left">
          <p class="headline">Purchased products</p>
          <?php
          $totalprice = 0;
          if(!empty($orderProducts)) {
            $noProducts = false;
            $totalprice = 0;
            $cartItemsReadOnly = true; //used in cartItem.php
            foreach ($orderProducts as $id => $qnty) {
              include("components/cartItem.php");
            }
          }
          ?>
          <h2 style="text-align:center">Total: <?= $totalprice ?> €</h2>
        </div>

        <div class="order-col">
          <p class="headline">Shipping address</p>
          <?php
          if(isset($_SESSION['user'])){
            $user = getUserDetails($_SESSION['user']);
          }
          $email = isset($user['email']) ? $user['email'] : "";
          $firstname = isset($user['firstname']) ? $user['firstname'] : "";
          $lastname = isset($user['lastname']) ? $user['lastname'] : "";
          $town = isset($user['town']) ? $user['town'] : "";
          $zipcode = isset($user['zipcode']) ? $user['zipcode'] : "";
          $street = isset($user['street']) ? $user['street'] : "";
          $housenumber = isset($user['housenumber']) ? $user['housenumber'] : "";
          $deliverymethod = isset($user['deliverymethod']) ? $user['deliverymethod'] : "";
          ?>
          <div class="personaldetails-box">
            <label for="email">Email</label>
            <p><?= $email ?></p>
            <label for="firstname">Firstname</label>
            <p><?= $firstname ?></p>
            <label for="lastname">Lastname</label>
            <p><?= $lastname ?></p>
            <label for="town">Town</label>
            <p><?= $town ?></p>
            <label for="zipcode">Zipcode</label>
            <p><?= $zipcode ?></p>
            <label for="street">Street</label>
            <p><?= $street ?></p>
            <label for="housenumber">Housenumber</label>
            <p><?= $housenumber ?></p>
            <label for="deliverymethod">Deliverymethod</label>
            <p><?php echo $deliverymethod == "dhl" ? "DHL" : "Express"; ?></p>
          </div>
        </div>
      </div>
    <?php
    } else {
      $notice = "No orders for this session found.";
    }
  } else {
    $notice = "No user found";
  }
  if (isset($notice)) {
    ?>
    <p class="headline"><?=$notice?></p>
    <?php
  }
  ?>
</div>

<?php include("components/footer.php"); ?>
