<?php
include("components/header.php");
echo '<p class="headline">Shopping Cart</p>';
if(isset($_SESSION['products'])){
  $productsArray = $_SESSION['products'];
  if(!empty($productsArray)) {
    $totalprice = 0; ?>
    <div class="cartbox-left">
    <?php
    foreach ($productsArray as $id => $qnty) {
      include ("components/cartItem.php");
    }
    ?>
    </div>

    <div class="cartbox-right">
      <div class="total">
        <p class="total-price">Total: <span id="totalPrice"><?=$totalprice?></span> Euro</p>
        <?php
        if(!isset($_SESSION['user']) or !userexists($_SESSION['user'])){
          ?>
          <a href="personal_details.php">Proceed to checkout</a>
          <?php
        }
        else {  ?>
          <a href="checkout.php">Proceed to checkout</a>
          <?php
        }
        ?>
      </div>
    </div>
    <?php
  }
  else {
    echo "<h3>Your cart is empty.</h3>";
    echo "<p><a href='home.php'>Click here</a> to find products.</p>";
  }
}
else {
  echo "<h3>Your cart is empty.</h3>";
  echo "<p><a href='home.php'>Click here</a> to find products.</p>";
}
include("components/footer.php");
?>
