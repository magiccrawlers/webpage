<?php
include("components/functions.php");
session_start();

if(isset($_GET['prod']) and isset($_SESSION['products'])){
      $id = htmlspecialchars($_GET['prod']);
      $productsArray = $_SESSION['products'];
      if(array_key_exists($id, $productsArray) and productExistsById($id)){
        $indexProd = $id;
        unset($productsArray[$indexProd]);
        $_SESSION['products'] = $productsArray;
      }
}
header("Location: cart.php");
?>
