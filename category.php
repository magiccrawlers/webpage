<?php
include("components/header.php");
  $catId = 1;
  if(isset($_GET['cat'])){
  $cat = htmlspecialchars($_GET['cat']);
    switch ($cat) {
    case 'wristbands':
        $catId = 1;
      break;
    case 'lighters':
        $catId = 2;
      break;
    case 'swissknives':
        $catId = 3;
      break;
    case 'watches':
        $catId = 4;
      break;
    default:
        header("Location: error.php");
      break;
    }
  }

  $headline = getCatheadline();
  echo '<p class="headline">'.$headline.'</p>';
  $page = 1;
  $pageCount = getProductsPageCount($catId);
  if (isset($_GET["page"])) {
    $page = htmlspecialchars($_GET["page"]);
    if ($pageCount < $page) {
      $page = $pageCount;
    }
    if ($page < 1) {
      $page = 1;
    }
  }

  getProducts($catId, $page - 1);
  ?>
  <div class="pageCount">Page <?= ($page)." of ".$pageCount ?></div>

  <div class="pageNumbering">
    <form class="" action="" method="post">
      <?php
      if ($pageCount != 1) { ?>
        <label for="page">Go to: </label>
        <input id="goToPageVal" type="number" name="page" value="<?= $page?>" min="1" max="<?= $pageCount ?>">
        <input id="goToPageBtn" type="submit" name="goto" formaction="category.php?cat=<?= $cat ?>&page=<?= $page > 0 ? $page : 1 ?>" value="Go">
        <?php
      } ?>
    </form>
  </div>
  <div class="pageNumbering">
    <form class="" action="" method="post">
      <?php if ($page != 1) { ?>
        <input type="submit" name="first" formaction="category.php?cat=<?= $cat ?>&page=1" value="First">
        <input type="submit" name="back" formaction="category.php?cat=<?= $cat ?>&page=<?= $page > 1 ? $page - 1 : 1 ?>" value="Previous">
        <?php
      }
      if ($page != $pageCount) { ?>
        <input type="submit" name="next" formaction="category.php?cat=<?= $cat ?>&page=<?= $page + 1 ?>" value="Next">
        <?php
      } ?>
    </form>
  </div>
  <?php
include("components/footer.php");
?>

<script>
  $('#goToPageBtn').click(function(){
    var goto = $('#goToPageVal').val();
    if (goto > <?= $pageCount?>) {
      goto = <?= $pageCount ?>;
    }
    $('#goToPageBtn').attr('formaction', 'category.php?cat=<?= $cat ?>&page=' + goto);
  });
</script>
