<?php
  include("components/header.php");
  if(isset($_SESSION['tmpUser'])){
    $user = $_SESSION['tmpUser'];
  } else if(isset($_SESSION['user'])){
    $user = getUserDetails($_SESSION['user']);
  }
  $email = isset($user['email']) ? $user['email'] : "";
  $firstname = isset($user['firstname']) ? $user['firstname'] : "";
  $lastname = isset($user['lastname']) ? $user['lastname'] : "";
  $town = isset($user['town']) ? $user['town'] : "";
  $zipcode = isset($user['zipcode']) ? $user['zipcode'] : "";
  $street = isset($user['street']) ? $user['street'] : "";
  $housenumber = isset($user['housenumber']) ? $user['housenumber'] : "";
  $creditnumber = isset($user['creditnumber']) ? $user['creditnumber'] : "";
  $securecode = isset($user['securecode']) ? $user['securecode'] : "";
  $deliverymethod = isset($user['deliverymethod']) ? $user['deliverymethod'] : "dhl";
?>
<p class="headline">Personal Details</p>

<div class="personaldetails-box">
  <form class="" action="personal_details_check.php" method="post">
    <input type="email" name="email" placeholder="Email" value="<?= $email ?>" required>
    <input type="text" name="firstname" placeholder="Firstname" value="<?= $firstname ?>" required>
    <input type="text" name="lastname" placeholder="Lastname" value="<?= $lastname ?>" required>
    <input type="text" name="town" placeholder="Town" value="<?= $town ?>" required>
    <input type="text" name="zipcode" placeholder="Zipcode" value="<?= $zipcode ?>" required>
    <input type="text" name="street" placeholder="Street" value="<?= $street ?>" required>
    <input type="text" name="housenumber" placeholder="Housenumber" value="<?= $housenumber ?>" required>
    <input type="text" name="creditnumber" placeholder="Creditnumber" value="<?= $creditnumber ?>" required>
    <input type="text" name="securecode" placeholder="Securecode" value="<?= $securecode ?>" required>
    <div class="selectQuantity rounded size-surrounding-area">
      <select name="deliverymethod" required>
        <option value="dhl"     <?php echo $deliverymethod == "dhl" ? "selected" : ""; ?>>Del. method DHL</option>
        <option value="express" <?php echo $deliverymethod == "express" ? "selected" : ""; ?>>Del. method Express</option>
      </select>
    </div>
    <input class="btnStandard" type="submit" name="submitpersonal" value="Submit">
    <input class="btnNegative" type="reset" name="resetpersonal" value="Reset">
  </form>
</div>

<?php
  if(isset($_SESSION['tmpUser'])){
    $error = $_SESSION['errorPersonalDetails'];
    echo "<script type=\"text/javascript\">
            alert(\"".$error."\");
          </script>";
  }
  include("components/footer.php");
?>
