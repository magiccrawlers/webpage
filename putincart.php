<?php
include("components/functions.php");
session_start();

$productsArray = array();
if(isset($_SESSION['products'])) {
  $productsArray = $_SESSION['products'];
}

if(isset($_POST['quantity'])){
    $quantity = $_POST['quantity'];
    if(isset($_GET['prod'])){
      $id = htmlspecialchars($_GET['prod']);
      if(productExistsById($id)){
        $productsArray[$id] = $quantity;
        $_SESSION['products'] = $productsArray;
        header("Location: product.php?prod=$id&suc=1");
      }
      else {
        header("Location: productError.php");
      }
    }
}
?>
