-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 11. Mai 2017 um 19:13
-- Server-Version: 10.1.21-MariaDB
-- PHP-Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `mensextra`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Categories`
--

CREATE TABLE `Categories` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE latin1_german1_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;

--
-- Daten für Tabelle `Categories`
--

INSERT INTO `Categories` (`id`, `name`) VALUES
(1, 'Wristbands'),
(2, 'Lighters'),
(3, 'Swiss Knives'),
(4, 'Watches');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `datetime` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `datetime`) VALUES
(8, 13, '2017-05-10 11:01:05'),
(9, 13, '2017-05-10 11:14:56'),
(10, 13, '2017-05-10 11:19:47'),
(11, 13, '2017-05-10 11:20:24'),
(12, 13, '2017-05-10 11:21:59'),
(13, 13, '2017-05-10 11:22:43'),
(14, 13, '2017-05-10 11:23:24'),
(15, 26, '2017-05-10 20:00:13');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `order_details`
--

CREATE TABLE `order_details` (
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `order_details`
--

INSERT INTO `order_details` (`order_id`, `product_id`, `quantity`) VALUES
(8, 5, 1),
(8, 6, 1),
(8, 7, 1),
(8, 14, 1),
(9, 5, 1),
(11, 5, 2),
(12, 5, 2),
(13, 5, 2),
(14, 5, 2),
(15, 2, 2),
(15, 3, 3),
(15, 7, 3);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Pictures`
--

CREATE TABLE `Pictures` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `picpath` varchar(256) COLLATE latin1_german1_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;

--
-- Daten für Tabelle `Pictures`
--

INSERT INTO `Pictures` (`id`, `product_id`, `picpath`) VALUES
(1, 2, 'Black_Leather_Braided_Bracelet.jpeg'),
(2, 3, 'Multilayer_Gold_Alloy_Anchor.jpg'),
(3, 4, 'Braided_Leather_Skull.jpg'),
(4, 5, 'Leather_Magnetic_Wrap_Skull.jpg'),
(5, 6, 'Veteran_Vietnam_Zippo.jpg'),
(6, 7, 'zippo_dragon.jpeg'),
(7, 8, 'zippo_jack_daniels.jpg'),
(8, 9, 'zippo_skull_mask.jpg'),
(9, 10, 'victorinox_huntsman_black.jpg'),
(10, 11, 'victorinox_hunter_pro_wood.jpg'),
(11, 12, 'victorinox_forester_wood.jpg'),
(12, 13, 'victorinox_trailmaster.jpg'),
(13, 14, 'watch_mens-mother-of-pearl-swiss-chronograph.jpg'),
(14, 15, 'watch_mens-mechanical-gold-tone-skeletal-dial-gold-tone-ss.jpg'),
(15, 16, 'watch_daredevil-chronograph-black-silicone-and-dial-stainless-steel.jpg'),
(16, 17, 'watch_mens-chronograph-gunmetal-dial-brown-genuine-leather.jpg');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Products`
--

CREATE TABLE `Products` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE latin1_german1_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `details` varchar(300) COLLATE latin1_german1_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `price_eur` decimal(7,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;

--
-- Daten für Tabelle `Products`
--

INSERT INTO `Products` (`id`, `name`, `category_id`, `details`, `stock`, `price_eur`) VALUES
(2, 'Braided Black Leather', 1, '1 x Stainless Steel Leather Braided Bracelet. Stainless Steel Leather Braided Bracelet. Ever-lasting design braided bracelet that will serve as your perfect accessory. Material: Leather and stainless steel', 1, '4.99'),
(3, 'Multilayer Gold Alloy Anchor', 1, 'Setting Type:Bezel SettingMaterial:LeatherChain Type:Rolo ChainLength:70-80cmClasp Type:Lace-upMetals Type:Gold PlatedShape\\pattern:Geometric', 4, '19.95'),
(4, 'Braided Leather Skull Bracelet', 1, 'Quantity 1x Bracelet 1x Gift Box. Material Leather. If the item does not satisfy you or does not work out for you - let us know. Color Black, Blue, Brown, Green, Gray, Red, Pink, White.', 12, '17.95'),
(5, 'Leather Magnetic Wrap Skull Bracelet', 1, 'Item arrives with a small jewelry bag Material:Leather Stainless Steel Color:black silver A stylish men\'s black leather skull bracelet.', 0, '13.95'),
(6, 'Veteran Vietnam Zippo', 2, 'Lighter has a matching Zippo insert PAT. 2517191 which sparks and just needs Zippo fluid to light up!', 9, '59.95'),
(7, 'Zippo with dragon emblem', 2, 'Zippo Windproof Lighter With Dragon Emblem<br>Unique Feature: Upon opening the lighter flames appear to symbolize a fire breathing dragon<br>Finish: High Polished Chrome<br>Name: Fire Breathing Dragon<br>Comes in a black velour box as shown in photo 4.<br>Dimensions: 2 1/4 inches high x 1 1/2 inch w', 4, '29.95'),
(8, 'Zippo Jack Daniels', 2, 'Genuine Zippo windproof lighter with distinctive Zippo \"click\"<br>All metal construction; windproof design works virtually anywhere<br>Refillable for a lifetime of use; for optimum performance, we recommend genuine Zippo premium lighter fluid, flints, and wicks.<br>Made in USA; lifetime guarantee<br', 20, '39.95'),
(9, 'Skull Mask', 2, 'Inspired by the Mexican celebration of Day of the Dead, this lighter features the dark face of a skull.<br><br>White Matte<br>Color Imaging<br>Lifetime guarantee<br>Lighter comes unfilled. Use Zippo premium lighter fluid (sold separately)<br>Genuine Zippo windproof lighter with distinctive Zippo \"cl', 2, '30.95'),
(10, 'Victorinox Huntsman black', 3, 'Attributes: wood saw, wire stripper, can opener with screwdriver, cap lifter with scewdriver, corkscrew, key ring, large blade, multi-purpose hook, reamer, punch, scissors, small blade, toothpick, tweezers', 0, '35.95'),
(11, 'Victorinox Hunter pro', 3, 'Attributes: Blade<br>The Hunter\'s Knife from Victorinox was developed from scratch as a special knife for hunters in the United States. The Hunter Pro is the result of this particular challenge. It comes with a large one-handed blade. The ergonomic shape lends the hunting knife an extremely stable g', 20, '119.95'),
(12, 'Victorinox Forester wood', 3, 'Attributes: 	wood saw, wire stripper, blade with safty lock system, can opener with screwdriver, cap lifter with scewdriver, corkscrew, key ring, reamer, punch, toothpick, tweezers', 100, '54.95'),
(13, 'Victorinox Trailmaster', 3, 'Attributes: wood saw, wire stripper, blade with safty lock system, can opener with screwdriver, cap lifter with scewdriver, key ring, phillips screwdriver (cross), reamer, punch, toothpick, tweezers', 1, '44.95'),
(14, 'Men\'s Mother of Pearl Swiss Chronograph', 4, 'The Akribos XXIV, Ultimate series features a black ion-plated stainless steel 45.5mm case, with a fixed black ion-plated inlaid with czech stones bezel, a black mother of pearl dial and a shatter resistant krysterna crystal.<br>Sub-dials: day of the week and date.<br>The 45.5mm black ion-plated stai', 4, '119.98'),
(15, 'Men\'s Mechanical Gold Tone Skeletal Dial Gold Tone', 4, 'The Akribos XXIV. The Watch features a gold-tone stainless steel 43mm case, with a fixed gold-tone coin-edged bezel, a gold-tone skeleton dial and a shatter resistant krysterna crystal.<br>Sub-dials: seconds subdial at the 9 o\'clock position.<br>The 43mm gold-tone stainless steel band is fitted with', 5, '389.95'),
(16, 'Daredevil Chronograph Black Silicone and Dial Stai', 4, 'The Swiss Legend, Daredevil series features a stainless steel 50mm case, with a inner tachymeter black ion plated stainless steel bezel, a black dial and a sapphitek crystal.<br>Chronograph - sub-dials displaying: 60 second, 30 minute and 1/10th of a second.<br>The 50mm stainless steel band is fitte', 5, '169.95'),
(17, 'Men\'s Chronograph Gunmetal Dial Brown Genuine Leat', 4, 'The Diesel, Avanced series features a stainless steel 51.6mm case, with a fixed stainless steel bezel, a grey dial and a scratch resistant mineral crystal.<br>Chronograph - sub-dials displaying: 60 second, 30 minute and 24 hour.<br>The 51.6mm stainless steel band is fitted with tang clasp.<br>This b', 2, '114.95');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(100) COLLATE latin1_german1_ci NOT NULL,
  `firstname` varchar(50) COLLATE latin1_german1_ci NOT NULL,
  `lastname` varchar(50) COLLATE latin1_german1_ci NOT NULL,
  `town` varchar(50) COLLATE latin1_german1_ci NOT NULL,
  `zipcode` varchar(16) COLLATE latin1_german1_ci NOT NULL,
  `street` varchar(100) COLLATE latin1_german1_ci NOT NULL,
  `housenumber` varchar(6) COLLATE latin1_german1_ci NOT NULL,
  `deliverymethod` varchar(25) COLLATE latin1_german1_ci NOT NULL,
  `creditnumber` varchar(20) COLLATE latin1_german1_ci NOT NULL,
  `securecode` varchar(3) COLLATE latin1_german1_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci ROW_FORMAT=COMPACT;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `email`, `firstname`, `lastname`, `town`, `zipcode`, `street`, `housenumber`, `deliverymethod`, `creditnumber`, `securecode`) VALUES
(5, 'majjuss@web.de', 'Marius', 'Meisterernst', 'Bolzano', '39100', 'Via Palermo', '3', 'dhl', '', ''),
(6, 'majjuss2@web.de', 'Marius', 'Meisterernst', 'Bolzano', '39100', 'Via Palermo', '3', 'dhl', '', ''),
(7, 'majjusstest@web.de', 'Marius', 'Meisterernst', 'Bolzano', '39100', 'Via Palermo', '3', 'dhl', '', ''),
(8, 'majjuss@web.de', 'Marius', 'Meisterernst', 'Bolzano', '39100', 'Via Palermo', '3', 'dhl', '', ''),
(9, 'majjuss@web.de', 'Marius', 'Meisterernst', 'Bolzano', '39100', 'Via Palermo', '3', 'dhl', '', ''),
(10, 'majjuss@web.de', 'Marius', 'Meisterernst', 'Bolzano', '39100', 'Via Palermo', '3', 'dhl', '', ''),
(11, 'majjuss@web.de', 'Marius2', 'Meisterernst', 'Bolzano', '39100', 'Via Palermo', '3', 'dhl', '', ''),
(12, 'majjuss@web.de', 'Marius', 'Meisterernst', 'Bolzano', '39100', 'Via Palermo', '3', 'dhl', '', ''),
(13, 'majjuss3@web.de', 'Marius', 'Meisterernst', 'Bolzano', '39100', 'Via Palermo', '3', 'dhl', '', ''),
(14, 'asdf@a.it', 'asdf', 'asf', 'asdf', '351235', 'asdfad', '23', 'dhl', '', ''),
(15, 'asdf@a.it', 'adsf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'dhl', '', ''),
(16, 'asdf@a.it', 'asdf', 'adsf', 'asdf', 'asdf', 'asdf', 'asdf', 'dhl', '', ''),
(17, 'asdf@a.it', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'dhl', '1234567890123', '756'),
(18, 'asdf@a.it', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'dhl', '1234567890123', '756'),
(19, 'asdf@a.it', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', '0', '1234567890123', '756'),
(20, 'asdf@a.it', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', '0', 'dhl', '1234567890123', '756'),
(21, 'asdf@a.it', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'dhl', '1234567890123', '756'),
(22, 'asdf@a.it', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'dhl', '1234567890123', '756'),
(23, 'asdf@a.it', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'dhl', '1234567890123', '756'),
(24, 'asdf@a.it', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'dhl', '1234567890123', '756'),
(25, 'asdf@a.it', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'dhl', '1234567890123', '756'),
(26, 'asdf@a.it', 'asdfqwer', 'ölkj', 'citi', '6789', 'oooweg', '99', 'dhl', '123456789014627', '245');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `Categories`
--
ALTER TABLE `Categories`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indizes für die Tabelle `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`order_id`,`product_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indizes für die Tabelle `Pictures`
--
ALTER TABLE `Pictures`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `Products`
--
ALTER TABLE `Products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk` (`category_id`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `Categories`
--
ALTER TABLE `Categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT für Tabelle `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT für Tabelle `Pictures`
--
ALTER TABLE `Pictures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT für Tabelle `Products`
--
ALTER TABLE `Products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT für Tabelle `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_order_details` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_order_details` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `Products`
--
ALTER TABLE `Products`
  ADD CONSTRAINT `fk` FOREIGN KEY (`category_id`) REFERENCES `Categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
