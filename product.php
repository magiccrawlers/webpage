<?php
  include("components/header.php");

if(isset($_GET['prod'])){
  $id = htmlspecialchars($_GET['prod']);
  if(productExistsById($id)){
    $product = getProductById($id);
    $main_pic_path = "pictures/".getPathById($id);
    $stock = $product->get_stock();
    ?>
    <p class="headline">Product Information</p>
    <div class="mainContainer">
      <div class="subContainer half">
        <div class="picture">
          <span><img src="<?= $main_pic_path ?>" alt="Picture"/></span>
        </div>
      </div>
      <div class="subContainer half">
        <form action="putincart.php?prod=<?=$id ?>" method="post">
          <div class="actionbox">
            <span class="name"><?= $product->get_name() ?></span>
            <span class="price"><?= $product->get_price() ?> Euro</span>
            <?php if($stock == 0): ?>
              <div class="red">This product is currently not available.</div>
            <?php else: ?>
              <p class="availability <?=setAvailabilityColor($stock)?>"><?php echo informAvailability($stock);?></p>
              <div class="addToCartComp">
                <div class="selectQuantity rounded">
                  <select name="quantity" autocomplete="off" id="quantity" tabindex="-1">
                    <?php for ($i=1; $i <= $product->get_stock(); $i++) {
                      echo '<option value="'.$i.'">'.$i.'</option>';
                    } ?>
                  </select>
                </div>
                  <input class="addToCartBtn rounded" type="submit" name="addtocart" value="Add to cart" />
              </div>
              <?php
              if(isset($_GET['suc'])){
              ?>
              <p class="green">Product added to your cart.</p>
              <?php
              }
              ?>
            <?php endif; ?>
          </div>
        </form>
      </div>
      <div class="subContainer text">
        <h2>Details</h2>
        <span class="details"><?= $product->get_details() ?></span>
      </div>
    </div>
    <?php
  }
  else {
    header("Location: productError.php");
  }
}
else {
  header("Location: error.php");
}
include("components/footer.php");
?>
