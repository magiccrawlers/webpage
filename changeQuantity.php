<?php
session_start();
include("components/functions.php");


if(isset($_POST['qnty']) and isset($_POST['id']) and isset($_SESSION['products'])){
  $qnty = htmlspecialchars($_POST['qnty']);
  $id = htmlspecialchars($_POST['id']);
  $product = getProductById($id);
  $stock = $product->get_stock();
  if($qnty > $stock or $qnty < 1 ){
    $qnty = $stock;
  }
  $subtotal = $qnty * $product->get_price();
  $productsArray = $_SESSION['products'];
  if(array_key_exists($id, $productsArray)){
    $productsArray[$id] = $qnty;
    $_SESSION['products'] = $productsArray;
  }
  $totalprice = 0;
  foreach ($productsArray as $pId => $q) {
    $p = getProductById($pId);
    $totalprice = $totalprice + ($p->get_price() * $q);
  }
  echo json_encode(array('id' => $id, 'qnty' => $qnty, 'subTotal' => $subtotal, 'totalPrice' => $totalprice));
}
?>
