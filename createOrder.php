<?php
  session_start();
  include("components/functions.php");

  if (!checkCheckout()) {
    header("Location: checkout.php");
  } else { // Call function to create order and redirect to order confirmation page.
    $productsArray = $_SESSION['products'];
    $user = getUserDetails($_SESSION['user']);
    $orderId = createOrderFromCart($user, $productsArray);
    if ($orderId > 0) {
      unset($_SESSION["products"]);
      header("Location: order.php");
    } else {
      header("Location: orderError.php");
    }
  }
  exit();
?>
