function changeQuantity(id, stock) {
  var qnty = document.getElementById('qnty'+id).value;
  if (qnty > stock) {
      alert("This product is only " + stock + " times available. Please choose another amount.");
      qnty = stock;
  }
  else {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        response = JSON.parse(this.responseText);
        document.getElementById("qnty"+response['id']).value = response['qnty'];
        document.getElementById("subtotal"+response['id']).innerHTML = response['subTotal'];
        document.getElementById("totalPrice").innerHTML = response['totalPrice'];
      }
    };
    xmlhttp.open("POST", "changeQuantity.php", true);
    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    params = "qnty=" + qnty + "&id=" + id;
    xmlhttp.send(params);
  }
}
