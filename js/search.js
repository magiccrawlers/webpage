//Asynchronous search function, realized with AJAX
function getSearchResult(str) {
  if (str.length == 0) {
    document.getElementById("searchInput").innerHTML = "";
    document.getElementById("navoutput").style.display = "none"
    return;
  } else {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("navoutput").innerHTML = this.responseText;
        document.getElementById("navoutput").style.display = str.length < 2 ? "none" : "block";
      }
      //Closes search div, if user clicks outside
      document.getElementById("mc").onclick = function () {
          document.getElementById("navoutput").style.display = "none";
      }
    };
    xmlhttp.open("GET", "searchProduct.php?searchVal="+str, true);
    xmlhttp.send();
  }
}
