$(document).ready(function(){
  $(".dropbtn").click(function(){
    $(".dropdown-content").slideToggle();
  });
});

$(document).ready(function(){
  $("#menu-icon").click(function(){
    $(".dropdownMobile").slideToggle();
  });
});

//Schließt bei klick in den Bereich unter der Navigationszeile die Suche.
$(document).ready(function(){
  $(".main").click(function(){
    $(".dropdown-content").hide();
    if ($(window).width() < 901) {
      $(".dropdownMobile").hide();
    }
  });
});
